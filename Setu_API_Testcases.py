import unittest
import requests
import jwt
import api


class MyTestCase(unittest.TestCase):

    jwt_token = ""
    id = ""
    URL = "http://localhost:3000"
    CALLBACK_URL = api.CALLBACK_URL

    @classmethod
    def setUpClass(cls):
        # Checking if url and callback_url are online
        try:

            r1 = requests.post(cls.URL)
            r2 = requests.get(cls.CALLBACK_URL)
            if r1.status_code != 403 and r2.status_code != 200:
                raise Exception('{} and {}'.format(r1.status_code,r2.status_code))
        except requests.exceptions.ConnectionError as c:
            unittest.TestCase().fail('Check if %s and %s are online!!!' %(cls.URL,cls.CALLBACK_URL))
        except Exception as e:
            unittest.TestCase().fail('Unexpected Response Code from %s and %s - %s'\
                                     % (cls.URL, cls.CALLBACK_URL,e))

    def testcase000(self):
        """
        Name : Testcase000
        Test API : /api/onboard
        Description : Testing /api/onboard with empty json input
        Pass Criteria : Should throw 400 Error
        """

        url = MyTestCase.URL + '/api/onboard'

        input_data = {
        }
        try:
            resp = requests.post(url, json=input_data)
            if resp.status_code == 400 and resp.json()['error'] == 'data error':
                print('testcase000 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase000 - FAILED %s' %e)


    def testcase001(self):
        """
        Name : Testcase001
        Test API : /api/onboard
        Description : Testing /api/onboard with empty key and empty values
        Pass Criteria : Should throw 400 Error
        """

        url = MyTestCase.URL + '/api/onboard'

        input_data = {
            "callbackURL": "",
            "name": ""
        }
        try:
            resp = requests.post(url, json=input_data)
            if resp.status_code == 400 and resp.json()['error'] == 'data error':
                print('testcase001 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase001 - FAILED %s' %e)



    def testcase002(self):

        """
        Name : Testcase002
        Test API : /api/onboard
        Description : Testing /api/onboard with name value empty
        Pass Criteria : Should throw 400 Error
        """

        url = MyTestCase.URL + '/api/onboard'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'

        input_data = {
            "callbackURL": callback_url,
            "name": ""
        }
        try:
            resp = requests.post(url, json=input_data)
            if resp.status_code == 400 and resp.json()['error'] == 'data error':
                print('testcase002 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase002 - FAILED %s' %e)

    def testcase003(self):
        """
        Name : Testcase003
        Test API : /api/onboard
        Description : Testing /api/onboard with expected input
        Pass Criteria : Response Code - 200 and Message - Success
        """

        url = MyTestCase.URL + '/api/onboard'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'

        input_data = {
            "callbackURL": callback_url,
            "name": "UltraPayer"
        }
        try:
            resp = requests.post(url, json=input_data)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                MyTestCase.id = str(resp.json()['data']['id'])
                MyTestCase.jwt_secreat =resp.json()['data']['jwtSecret']
                MyTestCase.jwt_token = jwt.encode({'some': 'payload'}, \
                                              resp.json()['data']['jwtSecret'], \
                                              algorithm='HS256').decode("utf-8")
                print('testcase003 - PASSED')

            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))
        except Exception as e:
            unittest.TestCase().fail('testcase003 - FAILED %s' %e)


    def testcase004(self):
        """
        Name : Testcase004
        Test API : /api/bills
        Description : Testing /api/bills with incorrect Payer-ID
        Pass Criteria : Response Code - 403 and Message - auth error
        """
        url = MyTestCase.URL + '/api/bills'
        headers = {
            "PAYER-ID": "0",
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }
        try:
            resp = requests.get(url, headers=headers)
            if resp.status_code == 403 and resp.json()['error'] == 'auth error':
                print('testcase004 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'.\
                                format(resp.status_code,resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase004 - FAILED %s' %e)


    def testcase005(self):
        """
        Name : Testcase005
        Test API : /api/bills
        Description : Testing /api/bills with invalid jwt token
        Pass Criteria : Response Code - 400 and Message - The algorithm is not supported
        """

        url = MyTestCase.URL + '/api/bills'
        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer 2323hdakjs" + MyTestCase.jwt_token
        }
        try:
            resp = requests.get(url, headers=headers)
            if resp.status_code == 400 and \
                    resp.json()['error'] == 'The algorithm is not supported!':
                print('testcase005 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase005 - FAILED %s' %e)


    def testcase006(self):
        """
        Name : Testcase006
        Test API : /api/bills
        Description : Testing /api/bills with different Payer-Id and Authorization key
        Pass Criteria : Response Code - 401 and Message - Invalid key
        """

        url = MyTestCase.URL + '/api/bills'
        headers = {
            "PAYER-ID": '10',
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }
        try:
            resp = requests.get(url, headers=headers)
            if resp.status_code == 401 and resp.json()['error'] == 'Invalid key!':
                print('testcase006 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['error']))
        except Exception as e:
            unittest.TestCase().fail('testcase006 - FAILED %s' %e)


    def testcase007(self):
        """
        Name : Testcase007
        Test API : /api/bills
        Description : Testing /api/bills with correct Payer-Id and Authorization key
        Pass Criteria : Response Code - 200 and Message - success
        """

        url = MyTestCase.URL + '/api/bills'
        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }
        try:
            resp = requests.get(url, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                print('testcase007 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))
        except Exception as e:
            unittest.TestCase().fail('testcase007 - FAILED %s' %e)


    def testcase008(self):

        """
        Name : Testcase008
        Test API : /api/pay
        Description : Testing /api/pay with empty input json and callback response payload
        Pass Criteria :-
        callback response - Success- False , Message - Bill not found and Response Code - 200
        Response Code - 200 and Message - success
        """

        url = MyTestCase.URL + '/api/pay'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'

        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }

        input_data = {
            "ifsc": "",
            "accountNumber": "",
            "amount": 10000
        }
        try:
            resp = requests.post(url, json=input_data, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                callback_response = requests.get(callback_url)
                callback_request_data = callback_response.json()
                self.assertFalse(callback_request_data['success'])
                self.assertEqual(callback_request_data['error'],'Bill not found')
                print('testcase008 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))
        except Exception as e:
            unittest.TestCase().fail('testcase008 - FAILED %s' %e)


    def testcase009(self):

        """
        Name : Testcase009
        Test API : /api/pay
        Description : Testing /api/pay with incorrect details
        Pass Criteria :-
        callback response - Success- False , Message - Bill not found and Response Code - 200
        Response Code - 200 and Message - success
        """
        url = MyTestCase.URL + '/api/pay'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'

        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }

        input_data = {
            "ifsc": "SETU000012",
            "accountNumber": "1912391798418",
            "amount": 10000
        }
        try:
            resp = requests.post(url, json=input_data, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                callback_response = requests.get(callback_url)
                callback_request_data = callback_response.json()
                self.assertFalse(callback_request_data['success'])
                self.assertEqual(callback_request_data['error'],'Bill not found')
                print('testcase009 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))

        except Exception as e:
            unittest.TestCase().fail('testcase009 - FAILED %s' %e)


    def testcase010(self):

        """
        Name : Testcase010
        Test API : /api/pay
        Description : Testing /api/pay with empty input json
        Pass Criteria :-
        callback response - Success- False , Message - Amount did not match and Response Code - 200
        Response Code - 200 and Message - success
        """
        url = MyTestCase.URL + '/api/pay'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'
        bills_url = MyTestCase.URL + '/api/bills'

        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }

        try:
            resp = requests.get(bills_url, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                input_data = {
                    "ifsc": resp.json()['data']['bills'][0]['ifsc'],
                    "accountNumber": resp.json()['data']['bills'][0]['accountNumber'],
                    "amount": 1
                }
            else :
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))
            resp = requests.post(url, json=input_data, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                callback_response = requests.get(callback_url)
                callback_request_data = callback_response.json()
                self.assertFalse(callback_request_data['success'])
                self.assertEqual(callback_request_data['error'],'Amount did not match')
                print('testcase010 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                            format(resp.status_code, resp.json()['message']))
        except Exception as e:
            unittest.TestCase().fail('testcase010 - FAILED %s' %e)

    def testcase011(self):
        """
        Name : Testcase011
        Test API : /api/pay
        Description : Testing /api/pay with empty input json
        Pass Criteria :-
        callback response - Success - true , Response Code - 200
        Response Code - 200 and Message - success
        """
        url = MyTestCase.URL + '/api/pay'
        callback_url = MyTestCase.CALLBACK_URL + '/api/notifications'
        bills_url = MyTestCase.URL + '/api/bills'

        headers = {
            "PAYER-ID": MyTestCase.id,
            "Authorization": "Bearer " + MyTestCase.jwt_token
        }

        try:
            resp = requests.get(bills_url, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                bill_json = resp.json()['data']['bills'][1]
                input_data = {
                    "ifsc": resp.json()['data']['bills'][1]['ifsc'],
                    "accountNumber": resp.json()['data']['bills'][1]['accountNumber'],
                    "amount": resp.json()['data']['bills'][1]['amount']
                }
            else :
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))

            resp = requests.post(url, json=input_data, headers=headers)
            if resp.status_code == 200 and resp.json()['message'] == 'success':
                callback_response = requests.get(callback_url)
                callback_request_data = callback_response.json()
                self.assertTrue(callback_request_data['success'])
                self.assertEqual(callback_request_data['bill'], bill_json)
                print('testcase011 - PASSED')
            else:
                raise Exception('Response Code {} , Error {}'. \
                                format(resp.status_code, resp.json()['message']))
        except Exception as e:
            unittest.TestCase().fail('testcase011 - FAILED %s' % e)

    @classmethod
    def tearDownClass(cls):
        pass

if __name__ == '__main__':
    unittest.main()
