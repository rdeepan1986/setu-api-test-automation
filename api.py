from flask import Flask, json, request

CALLBACK_URL = "http://localhost:5000"
CALLBACK_HOST = 'localhost'
CALLBACK_PORT = 5000
notification_request = {}
api = Flask(__name__)

@api.route('/api/notifications', methods=['POST'])
def do_post():
    global notification_request
    notification_request = request.get_json()
    return json.dumps({"success": True}), 200

@api.route('/api/notifications', methods=['GET'])
def do_get():
    global notification_request
    return notification_request

@api.route('/', methods=['GET'])
def do_get2():
    return json.dumps({"Callback Server Online": True}), 200

    # Starting the Http Server
if __name__ == '__main__':
    api.run(host=CALLBACK_HOST,port=CALLBACK_PORT,debug=True)



