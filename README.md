# Setu API Test Automation

Python Automation script to Test Setup Sample API

Prerequisites:- 

Python 3.8 

Required python packages in the environment :- 

1. requests (pip install requests)
2. jwt (pip install pyjwt)
3. flask (pip install flask)

Steps to start test automation :- 

1. Start the node - node setuAPIs.js
2. Open a new terminal or command prompt
3. git clone the code 
    *  https://gitlab.com/rdeepan1986/setu-api-test-automation.git
4. cd setu-api-test-automation
5. Start callback server 
    * python api.py  
6. In another new terminal/command prompt start the test script
    * python Setu_API_Testcases.py  

